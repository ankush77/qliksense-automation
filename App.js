const express = require("express");
var fs = require("fs");
var request = require("request");
var path = require("path");

var directory = path.join(__dirname + "/cert/");

var app = express();

app.use(express.static(__dirname + "/public"));

var router = express.Router();
// test route
router.get("/", function (req, res) {
  res.json({ message: "Test Route" });
});

var get_ticket = function (userID, userDirectory, callback) {
  let proxyRestUri = "https://qlikdemo.polestarllp.com:4243/qps/virproxy/";
  var options = {
    uri: proxyRestUri + "ticket?xrfkey=0MwkYLXpxHrbkkGu",
    headers: {
      "content-type": "application/json",
      "X-Qlik-xrfkey": "0MwkYLXpxHrbkkGu",
      "X-Qlik-user": "UserDirectory=" + userDirectory + ";UserId=" + userID,
    },
    method: "POST",
    body: {
      UserDirectory: userDirectory,
      UserId: userID,
      Attributes: [],
    },
    json: true,
    ca: fs.readFileSync(directory + "server.pem"),
    key: fs.readFileSync(directory + "client_key.pem"),
    cert: fs.readFileSync(directory + "client.pem"),
    rejectUnauthorized: false,
  };
  request(options, function (error, response, body) {
    if (error) {
      console.log("Error: " + error);
      console.log(response);
      callback(-1);
    } else {
      console.log("--------Got a ticket--------");
      console.log("Ticket: " + response.body.Ticket);
      callback(response.body.Ticket);
    }
  });
};

app.use("/api", router);


process.env.NODE_TLS_REJECT_UNAUTHORIZED = "1"; 

function getOptions(path,ticket,method,data)
{
  var qrs_endpoint="https://qlikdemo.polestarllp.com:443/virproxy/qrs/"+path+'?qlikTIcket='+ticket+'&xrfkey=0123456789abcdef';
  // var qrs_endpoint="https://qlikdemo.polestarllp.com:4242/qrs/"+path+'?xrfkey=0123456789abcdef';
  var options = {
    uri: qrs_endpoint,
    headers: {
    'X-Qlik-xrfkey': '0123456789abcdef',
    'X-Qlik-user': 'UserDirectory=POLESTAR;UserId=qlikproductsdev'
      },
    method: method,
    body:data ,
  json: true,
    ca: fs.readFileSync(directory+ "server.pem"),
    key: fs.readFileSync(directory+"client_key.pem"),
    cert: fs.readFileSync(directory+"client.pem"),
    rejectUnauthorized: false
  };

  return options;
 
}

app.post("/copyApp", function (req, res) {
  get_ticket("qlikproductsdev", "POLESTAR", function (qlikTicket) {
    console.log(" Qlik Ticket : " + qlikTicket);
    let proxyRestUri =
      "https://qlikdemo.polestarllp.com/virproxy/api/hub/v1/apps/1bde987e-be4a-46af-955c-a8fd85eb7731/duplicate?qlikticket=" +
      qlikTicket;
    var options = {
      uri: proxyRestUri,
      method: "POST",
      json: true,
      rejectUnauthorized: false,
    };
    request(options, function (error, response, body) {
      if (error) {
        console.log("Error from duplicate function: " + error);
        console.log(response);
        res.send(-1);
      } else {
        console.log("--------Duplicated--------");
        get_ticket("qlikproductsdev", "POLESTAR", function (qlikTicket) {
          console.log(" Qlik Ticket : " + qlikTicket);
          var appId = response.body.data
          let proxyRestUri =
            "https://qlikdemo.polestarllp.com/virproxy/api/hub/v1/apps/"+appId.id+"/publish?qlikticket=" + 
			qlikTicket;
          var options2 = {
            uri: proxyRestUri,
            method: "POST",
            body: {
              streamId: "42a302a3-04f8-470c-9c23-52d7be960793",
              appName: "Copy1 Of Consumer Sales",
            },
            json: true,
            rejectUnauthorized: false,
          };
          request(options2, function (error2, response2, body) {
            if (error2) {
              console.log("Error from duplicate function: " + error2);
              console.log(response2);
              res.send(-1);
            } else {
              console.log("--------Published--------");
              res.send(response2);
            }
          });
        });
      }
    });
  });
});

app.post("/createNewStream", function (req, res) {
 get_ticket("qlikproductsdev", "POLESTAR", function (qlikTicket) {
  payload={
    "name":"Testing"
   };
   options = getOptions('stream',qlikTicket,'post',payload);
    request(options, function (error, response, body) {
        if(error) {
            console.log('Error: '+error);
            console.log(response);
            res.send(response)
        } else {   
		     res.send(response) 
			
        }
     });
   });
 });

 app.post("/createReloadTask", function (req, res) {
  get_ticket("qlikproductsdev", "POLESTAR", function (qlikTicket) {
   payload={
    	"task": {"app": {
        "id": "ee2aaaeb-7d6a-4b49-8526-43e963f11f11"
        },
       "name": "Reload task for app xyz"
      }  
   };
    options=getOptions('reloadtask/create',qlikTicket,'post',payload);
    request(options, function (error, response, body) {
        if(error) {
            console.log('Error: '+error);
            console.log(response);
        } else {       
            res.send(response) 
        }
    }); 
   });
});




//Added by ankush

app.post("/automate", function (req, res) {
 get_ticket("qlikproductsdev", "POLESTAR", function (qlikTicket) {
  payload={
    "name":"Testing1"
   };
   options = getOptions('stream',qlikTicket,'post',payload);
    request(options, function (error, response, body) {
        if(error) {
            console.log('Error: '+error);
            console.log(response);
            res.send(response)
        } else {   
		var StreamID = response.body.id;
            //res.send(response) 
			get_ticket("qlikproductsdev", "POLESTAR", function (qlikTicket) {
            console.log(" Qlik Ticket : " + qlikTicket);
			let proxyRestUri =
			"https://qlikdemo.polestarllp.com/virproxy/api/hub/v1/apps/1bde987e-be4a-46af-955c-a8fd85eb7731/duplicate?qlikticket=" +
			qlikTicket;
	var options = {
      uri: proxyRestUri,
      method: "POST",
      json: true,
      rejectUnauthorized: false,
    };
    request(options, function (error, response, body) {
      if (error) {
        console.log("Error from duplicate function: " + error);
        console.log(response);
        res.send(-1);
      } else {
        console.log("--------Duplicated--------");
        get_ticket("qlikproductsdev", "POLESTAR", function (qlikTicket) {
          console.log(" Qlik Ticket : " + qlikTicket);
          var appId = response.body.data
          let proxyRestUri =
            "https://qlikdemo.polestarllp.com/virproxy/api/hub/v1/apps/"+appId.id+"/publish?qlikticket=" + 
			qlikTicket;
          var options2 = {
            uri: proxyRestUri,
            method: "POST",
            body: {
              streamId: StreamID,
              appName: "Consumer Sales Testing",
            },
            json: true,
            rejectUnauthorized: false,
          };
          request(options2, function (error2, response2, body) {
            if (error2) {
              console.log("Error from duplicate function: " + error2);
              console.log(response2);
              res.send(-1);
            } else {
              console.log("--------Published--------");
              res.send(response2);
            }
          });
        });
      }
    });
  });
			
        }
     });
   });
 });

//end



app.listen(6000, function () {
  console.log("Listening on port 6000");
});
